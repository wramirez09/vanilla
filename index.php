<?php include("header.php") ?>
    <div class="jumbotron home_hero">
      <div class="container">
        <h1>Sent From Above Entertainment</h1>
        <p>dummy site for fun. sit amet, consectetur adipiscing elit. Nunc quis porttitor ante. Praesent ultrices nibh ultrices, bibendum quam eu, sollicitudin dolor. Etiam leo nisl, blandit non quam at, aliquet consequat.</p>
        <p><a class="btn btn-primary btn-lg" role="button">Learn more &raquo;</a></p>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-4">
          <img src="img/hero_2.jpg" alt="..." class="img-circle">
          <h2>Club Promotins</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-primary" href="#" role="button">View details &raquo;</a></p>
        </div>
        <div class="col-md-4">
          <img src="img/round_2.jpg" alt="..." class="img-circle">
          <h2>Event Planning</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-primary" href="#" role="button">View details &raquo;</a></p>
       </div>
        <div class="col-md-4">
          <img src="..." alt="..." class="img-circle">
          <h2>Market Exposure</h2>
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p><a class="btn btn-primary" href="#" role="button">View details &raquo;</a></p>
        </div>
      </div>
      <?php include("footer.php")?>
